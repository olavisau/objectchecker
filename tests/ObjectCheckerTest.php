<?php

use PHPUnit\Framework\TestCase;

use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\StringLength;

use Mercans\Assessment\ObjectChecker;
use Mercans\Assessment\AddressObjectChecker;

class ObjectCheckerTest extends TestCase
{
    public function testSimpleValidObject()
    {
        $object = new \stdClass();
        $object->name = "John Appleseed";
        $object->age = "12";

        $checker = new ObjectChecker($object);
        $checker->addValidator('name', new StringLength(['min' => 2]));
        $checker->addValidator('age', new Numericality());

        $this->assertTrue($checker->isValid());
    }

    public function testSimpleInvalidObject()
    {
        $object = new \stdClass();
        $object->name = "a";
        $object->age = "twelve";

        $checker = new ObjectChecker($object);
        $checker->addValidator('name', new StringLength(['min' => 2]));
        $checker->addValidator('age', new Numericality());

        $this->assertFalse($checker->isValid());
    }

    public function testNestedValidObject()
    {
        $address = new \stdClass();
        $address->country = "EE";

        $object = new \stdClass();
        $object->name = "John Appleseed";
        $object->age = "12";
        $object->address = $address;

        $checker = new ObjectChecker($object);
        $checker->addValidator('name', new StringLength(['min' => 2]));
        $checker->addValidator('age', new Numericality());
        $checker->registerObject('address', AddressObjectChecker::class);

        $this->assertTrue($checker->isValid());
    }

    public function testNestedInvalidObject()
    {
        $address = new \stdClass();
        $address->country = "SA";

        $object = new \stdClass();
        $object->name = "John Appleseed";
        $object->age = "12";
        $object->address = $address;

        $checker = new ObjectChecker($object);
        $checker->addValidator('name', new StringLength(['min' => 2]));
        $checker->addValidator('age', new Numericality());
        $checker->registerObject('address', AddressObjectChecker::class);

        $this->assertFalse($checker->isValid());
    }

    public function testChangedSimpleValidObject()
    {
        $object = new \stdClass();
        $object->name = "John Appleseed";
        $object->age = "12";

        $checker = new ObjectChecker($object);
        $checker->addValidator('name', new StringLength(['min' => 2]));
        $checker->addValidator('age', new Numericality());

        $this->assertTrue($checker->isValid());

        $object->name = "John";

        $this->assertTrue($checker->isValid());


    }

    public function testMultipleSimpleInvalidObject()
    {
        $object = new \stdClass();
        $object->name = "a";
        $object->age = "twelve";

        $checker = new ObjectChecker($object);
        $checker->addValidator('name', new StringLength(['min' => 2]));
        $checker->addValidator('age', new Numericality());

        $this->assertFalse($checker->isValid());

        $object->name = "c";

        $this->assertFalse($checker->isValid());
    }

    public function testMultipleNestedValidObject()
    {
        $address = new \stdClass();
        $address->country = "EE";

        $object = new \stdClass();
        $object->name = "John Appleseed";
        $object->age = "12";
        $object->address = $address;

        $checker = new ObjectChecker($object);
        $checker->addValidator('name', new StringLength(['min' => 2]));
        $checker->addValidator('age', new Numericality());
        $checker->registerObject('address', AddressObjectChecker::class);

        $this->assertTrue($checker->isValid());

        $object->name = "John";

        $this->assertTrue($checker->isValid());
    }

    public function testMultipleNestedInvalidObject()
    {
        $address = new \stdClass();
        $address->country = "SA";

        $object = new \stdClass();
        $object->name = "John Appleseed";
        $object->age = "12";
        $object->address = $address;

        $checker = new ObjectChecker($object);
        $checker->addValidator('name', new StringLength(['min' => 2]));
        $checker->addValidator('age', new Numericality());
        $checker->registerObject('address', AddressObjectChecker::class);

        $this->assertFalse($checker->isValid());

        $address->country = "GB";

        $this->assertFalse($checker->isValid());
    }
    public function testMultipleSimpleMixedObject()
    {
        //new $validator($validator->getOptions()) ANOTHER BUG, getOptions not implemented...
        $object = new \stdClass();
        $object->name = "a";
        $object->age = "twelve";

        $checker = new ObjectChecker($object);
        $checker->addValidator('name', new StringLength(['min' => 2]));
        $checker->addValidator('age', new Numericality());

        $this->assertFalse($checker->isValid());

        $object->name = "Adam";

        $this->assertTrue($checker->isValid());
    }


    public function testMultipleNestedMixedObject()
    {
        $address = new \stdClass();
        $address->country = "SA";

        $object = new \stdClass();
        $object->name = "John Appleseed";
        $object->age = "12";
        $object->address = $address;

        $checker = new ObjectChecker($object);
        $checker->addValidator('name', new StringLength(['min' => 2]));
        $checker->addValidator('age', new Numericality());
        $checker->registerObject('address', AddressObjectChecker::class);

        $this->assertFalse($checker->isValid());

        $address->country = "EE";

        $this->assertTrue($checker->isValid());
    }

}
