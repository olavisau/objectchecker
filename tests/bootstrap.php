<?php
$loader = new Phalcon\Loader();

$loader->registerNamespaces(
    ['Mercans\Assessment' => 'src/']
)->register();

$di = new Phalcon\Di();
$di->set ('validation', 'Phalcon\Validation');

Phalcon\Di::setDefault($di);
