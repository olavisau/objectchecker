# Implement ObjectChecker

## Prerequisites

* PHP 5.6+
* Phalcon 2.0.x

## Expected usage of the `ObjectChecker` class
The goal of the `ObjectChecker` class is to:

* Allow binding validators to object fields
* Run the validation using Phalcon's validation mechanism
* Provide access to the validation messages

The validators can be bound to object field as follows:

    $checker­>addValidator('name', new StringLength(['min' => 2]));
    $checker­>addValidator('age', new Numericality());

Note: The validator classes `StringLength` and `Numericality` are implemented by Phalcon, in the `Phalcon\Validation\Validator` namespace.

The object can then be validated by calling `$checker­>isValid()`, which returns a boolean value indicating the validness of the object being checked.

For example, the following object would be considered as valid:

    object(stdClass)#1 (2) {
        ["name"]=>
            string(14) "John Appleseed"
        ["age"]=>
            string(2) "12"
    }

Whereas the following would be considered invalid:

    object(stdClass)#2 (2) {
        ["name"]=>
            string(1) "a"
        ["age"]=>
            string(6) "twelve"
    }

When an object does not pass the validation, the error messages can be accessed by calling `$checker­>getMessages()`:

    foreach ($checker­>getMessages() as $message) {
        echo $message­>getField() . ': ' . $message­>getMessage();
    }

For example, in the case of the example object #2, the output would be something like this:

    name: Field name must be at least 2 characters long
    age: Field age does not have a valid numeric format
 
The `ObjectChecker` can also be used for validating nested objects such as the following:

    object(stdClass)#3 (3) {
    ["name"]=>
        string(14) "John Appleseed"
    ["age"]=>
        string(2) "12"
    ["address"]=>
        object(stdClass)#1 (1) {
            ["country"]=>
                string(2) "SA"
        }
    }

First, a helper class needs to be created for validating the "`address`" sub­object:

    class AddressObjectChecker extends ObjectChecker {
        public function __construct($obj) {
            parent::__construct($obj);
            $this­>addValidator('country', new InclusionIn([
                'domain' => ['EE', 'US', 'SA'],
            ]));
        } 
    }

Then the helper class can be assigned to the "`address`" property:

    $checker­>registerObject('address', 'AddressObjectChecker');

Now calling `$checker­>isValid()` will validate the properties name, age and country. Using registerObject in such way enables performing validation even on very large and deeply­nested structures.

## Additional information
The `ObjectChecker` must implement `ObjectCheckerInterface` (you are not allowed to change the interface).
Creation of additional helper methods / classes is allowed as long as the `ObjectChecker` class behaves as described in this document.

## Useful resources
Phalcon installation instructions: 
    https://phalconphp.com/en/download

Phalcon's validation mechanism documentation:
    https://docs.phalconphp.com/en/latest/reference/validation.html

Source code of Phalcon's validation mechanism:
    https://github.com/phalcon/cphalcon/tree/master/phalcon/validation
