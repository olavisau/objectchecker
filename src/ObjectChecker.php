<?php

namespace Mercans\Assessment;
use \Phalcon\Validation\Validator;
use \Phalcon\Validation\Message;
use \Phalcon\Validation\Message\Group;
use Phalcon\Di\InjectionAwareInterface;

class ObjectChecker implements ObjectCheckerInterface, InjectionAwareInterface
{
    /**
     * @var mixed $object
     */
    protected $object = null;

    /**
     * @var array $validationFields
     */
    protected $validationFields = [];

    /**
     * @var \Phalcon\DiInterface
     */
    protected $di = null;

    /**
     * //there's no interface?
     * @var \Phalcon\Validation $validator
     */
    protected $validator = null;

    /**
     * @var \Phalcon\Validation\Message\Group $messages
     */
    protected $messages = null;

    /**
     * ObjectChecker constructor.
     * @param $obj
     */
    public function __construct($obj)
    {
        $this->__constructor($obj);
    }

    /**
     * @param object $obj
     */
    public function __constructor($obj)
    {
        $this->object = $obj;
    }

    /**
     * @param string $field
     * @param \Phalcon\Validation\Validator $validator
     */
    public function addValidator($field, Validator $validator)
    {
        $this->validationFields[$field][] = $validator;
    }

    /**
     * @param string $field
     * @param string $checkerClass
     */
    public function registerObject($field, $checkerClass)
    {
        $this->validationFields[$field][] = new $checkerClass($this->object->$field);
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        $this->validator = $this->getDi()->get('validation');
        $fields = [];
        $messages = [];
        foreach($this->validationFields as $field => $validators){
            $fields[$field] = $this->object->$field;
            foreach($validators as $validator) {
                if($validator instanceof ObjectCheckerInterface) {
                    if(!$validator->isValid()){
                        //Due to bug! https://github.com/phalcon/cphalcon/issues/10405
                        foreach($validator->getMessages() as $message){
                            $messages[] = $message;
                        }
                    }
                    continue;
                }
                //new $validator($validator->getOptions()) ANOTHER BUG, getOptions not implemented...
                $this->validator->add($field, $validator);
            }
        }
        $this->messages = $this->validator->validate($fields);
        $this->appendMessages($messages);

        return !$this->messages->count();
    }

    /**
     * @return \Phalcon\Validation\Message\Group
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param \Phalcon\DiInterface $di
     */
    public function setDi( \Phalcon\DiInterface $di)
    {
        $this->di = $di;
    }


    public function getDi()
    {
        if(!$this->di) $this->di = \Phalcon\Di::getDefault();
        return $this->di;
    }

    /**
     * Append other messages to validator.
     * @param \Traversable $messages
     */
    protected function appendMessages( $messages){
        foreach($messages as $message){
            $this->validator->appendMessage($message);
        }
    }
}
